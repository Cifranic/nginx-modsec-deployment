#!/bin/bash 
# this script installs the certs 
# source: https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-centos-7

# if less than two arguments supplied, display usage 
	if [  $# -le 1 ] 
	then 
		printf "Improper script usage"
		printf "\n Example usage:\n $ bash 01-nginx-certs.sh www.mydomain.com mydomain.com\n"
		exit 1
	fi 
clear
printf "Installing dependencies, please wait...\n\n"
# get dependencies:
yum -y -q install python2-certbot-nginx

# get nslookup
yum -y -q install bind-utils

clear
echo $@
# store arguments in a special array 
args=("$@") 

# get number of elements 
ELEMENTS=${#args[@]} 


printf "\nPlease ensure that domains below are what you'd like to create a certificate for.\n Also, ensure they are spelled correctly): \n" 
 
# echo each element in array  
# for loop 
for (( i=0;i<$ELEMENTS;i++)); do 
    printf "  $i: ${args[${i}]} \n"
done

# space
echo ""

# prompt user to check if A records created for the domains.
read -r -p "Have you created A Records for the domains above? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY]) 
        printf "\nGreat! Continuing...\n"
        sleep 5
        clear
        ;;
    *)
        printf "\nEXITING SCRIPT!"
        printf "\nPlease first create A records with your domain provider, then rerun this script."
        exit 1
        ;;
esac

printf "Issuing a domain lookup for provided domains..."
sleep 3
echo ""

# Forloop to nslookup the addresses
for (( i=0;i<$ELEMENTS;i++)); do 
    #printf "\n HOST: ${args[${i}]} resolves to the addresse(s) below, does this look correct?\n"
    printf "\n HOST: ${args[${i}]}\n"
    nslookup ${args[${i}]} | grep "Address: " 
done

# prompt user for domain names
read -r -p "Are you sure these IP addresses are correct? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY]) 
        printf "\nGreat! Continuing...\n"
        sleep 5
        ;;
    *)
        printf "\nEXITING SCRIPT!"
        printf "\nTry waiting for the domain name to propigate, then try again later."
        exit 1
        ;;
esac


# insert the domain names 
domains=$(echo $@)

# replace the "localhost with the proper domain names"
sed -i "s/localhost;/$domains;/g" /usr/local/nginx/conf/nginx.conf

# allow certbox to update nginx.conf
#certbot --nginx --nginx-server-root /usr/local/nginx/conf --nginx-ctl /usr/local/nginx/sbin/nginx -d server.popcorn.blue -d www.server.popcorn.blue

# Store certbox command in variable... will append the domain name in a few lines..
command="certbot --nginx --nginx-server-root /usr/local/nginx/conf --nginx-ctl /usr/local/nginx/sbin/nginx"

#create proper certbot command (it will add the domain names, and create the cert
for (( i=0;i<$ELEMENTS;i++)); do
    printf "  $i: ${args[${i}]} \n"
    command="$command -d ${args[${i}]}"
done

clear
sleep 3
printf "\nThe last step is for you to copy the certbot command below and paste into your terminal. Ensure that you are running as root:\n\n"
printf "\n-----------\n"
echo $command
printf "\n-----------\n"
sleep 3




