#!/bin/bash

# installing AIDE
yum -y install aide

# ensure bot nginx root and web root are being monitored by aide
echo /home/pm2/ CONTENT_EX >> /etc/aide.conf
echo /usr/local/nginx/ CONTENT_EX >> /etc/aide.conf

# creating a conjob to run AIDE daily
echo "0 0 * * * root /usr/sbin/aide --check" >> /etc/crontab

# initializing the database
echo "initializing database... this could take a few minutes"
sleep 5s
aide --init 

# formatting the database 
mv /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz


# clear screen for fist check
clear

# run first check 
echo "Running first check... this could take a few minutes"
sleep 5s
aide --check

