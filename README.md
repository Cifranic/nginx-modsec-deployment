# secure-nginx-deployment
Ensure you run these scripts as root.

```bash
cd && yum -y install git && git clone https://gitlab.com/Cifranic/nginx-modsec-deployment.git
```
## Step1: Install nginx, ModSecurity, then add OWASP rules to ModSec:
The script below may take several minutes to complete. 
```bash
cd ~/nginx-modsec-deployment/
bash 00-deploy-nginx.sh
```
==================================================
==================================================

## Step2: Perform Manual checks:


**Manual Checks**


Ensure nginx is running: 
```bash
systemctl status nginx
```

Verify nginx installed correctly, and using modSec plugin:
```bash
/usr/local/nginx/sbin/nginx -V
```

SHOULD SEE THIS OUTPUT:
```
      nginx version: nginx/1.16.1
      built by gcc 7.3.1 20180712 (Red Hat 7.3.1-6) (GCC) 
      built with OpenSSL 1.0.2k-fips  26 Jan 2017
      TLS SNI support enabled
      configure arguments: --add-module=../modsecurity-2.9.3/nginx/modsecurity --with-http_ssl_module --with-http_stub_status_module

```
==================================================
==================================================

## Step3:  Enable https on nginx ModSecurity deployment 

Add your domain names into the 01-nginx-certs.sh file, as paramerter.  

```bash
cd ~/nginx-modsec-deployment/
bash 01-nginx-certs.sh <YOUR_DOMAIN_1> <YOUR_DOMAIN_2>
```
For example if my domain was example.com:

```bash
$ bash 01-nginx-certs.sh example.com www.example.com
```
Would create certs for both example.com AND www.example.com

Copy and paste the certbot command shown on your terminal... here is an example of what it looks like:
EX: certbot --nginx --nginx-server-root /usr/local/nginx/conf --nginx-ctl /usr/local/nginx/sbin/nginx -d www.example.com -d example.com


- Enter email address.
- Type 'A' to agree.
- Type (Y/N) to share email with EFF. 
- Type '2' and hit enter to redirect port 80 traffic to 443 (http->https)

Congrats! Your nginx instance is now protected with SSL and ModSecurity WAF!

To test that everything looks good, open your prefered web browser, and go to your new domain:  https://www.<your-domain>.com
NOTE: You will see "Error has occured", because we have not yet started our node.js apps yet.

==================================================
==================================================
## Step4:  Enable automatic SSL certificate

SSL certificates expire within 3 months of its original issue. The pupose of this script is to create an automated cronjob to refresh the certificate daily at 3:10AM. This is per best practice, as when it is a few weeks from expiring, it will successfully refresh the SSL certs. These attempts will also be logged in location: /var/log/cert-renew.log

To do this, run the 03-auto-refresh-certs.sh script: 

```bash
cd ~/nginx-modsec-deployment/
bash 03-auto-refresh-certs.sh
```

Ensure that the cronjob has been scheduled by issuing the command: 

```bash
crontab -l
```

Your output should be:
```bash
12 3 * * *   letsencrypt renew >> /var/log/cert-renew.log
```



## Step5:  Monitoring nginx with nginx amplify

![Nginx Dashboard](https://gitlab.com/Cifranic/nginx-modsec-deployment/raw/bfe57066c25c1acf912b3d654f791b8bcc00c232/captures/nginx-amp1.png)



# Features of nginx apmplify:
NGINX Amplify is a SaaS‑based monitoring tool for the open source NGINX software and NGINX Plus. With NGINX Amplify you can monitor performance, keep track of infrastructure assets, and improve configuration with static analysis. NGINX Amplify also monitors the underlying OS, application servers (like PHP‑FPM), databases, and other components. NGINX Amplify is simple to set up, yet powerful enough to provide critical insight into NGINX and system performance. 

This is a manual step because you must create an account and associate private access keys. Nginx apmlify offers this free service for users with less than 4 nginx servers.  Luckily for us, we're only using 1! 

**NOTE: Skip the purple box in a few steps
1. Create an account at: https://amplify.nginx.com/signup/
2. Click “+ New System” and follow the agent install instructions: Will display 2 commands in the green box to issue on your centos 7 server, skip the purple box, and you're good to go!   
        
After running the two commands above, start the nginx amplify agent and ensure it will run on startup  
```bash
systemctl start amplify-agent
systemctl enable amplify-agent
```

Now access your dashboard at https://amplify.nginx.com!  It will take a few minutes for metrics to populate. 
![Nginx Dashboard2](https://gitlab.com/Cifranic/nginx-modsec-deployment/raw/bfe57066c25c1acf912b3d654f791b8bcc00c232/captures/nginx-amp2.png)

==================================================
==================================================
## Step6: Filesystem integrity monitoring with AIDE  

AIDE description: AIDE File Integrity. AIDE (Advanced Intrusion Detection Enviornment) is a tool to check the file integrity. It is an opensource substitute for TRIPWIRE. It allows to take snapshots of all the major configuration files, binaries as well as libraries stats.

# installing and configuring aide
  - keeps a watchful eye over nginx and node.js web-root to ensure no one is tampering with nginx configs and system as a whole. 
  - Note: could take up to 10 minutes to run this script because it scans the entire system 

install aide 
```bash
cd ~/nginx-modsec-deployment/
bash 04-configure-AIDE.sh
```

Output should match: 

```
AIDE, version 0.15.1

### All files match AIDE database. Looks okay!
```

To test that AIDE is working properly, try creating a new file in /usr/local/nginx/, then running the aide --check command
```bash
touch /usr/local/nginx/new_file
```

You can also add some random data to the end of the README.md inside the node.js webroot
```bash
echo "a random change in the README.md file" >> /home/pm2/README.md
```

Run aide check again, this time, it will detect that the file system in our root nginx directory has been tampered with. This will take a few minutes to run.  
```
aide --check
```

you should see this output indicating that a new file was added, and that crontab has been changed:
```
AIDE 0.15.1 found differences between database and filesystem!!
Start timestamp: 2020-05-22 15:08:34

Summary:
  Total number of files:        141643
  Added files:                  1
  Removed files:                0
  Changed files:                1


---------------------------------------------------
Added files:
---------------------------------------------------

added: /usr/local/nginx/new_file

---------------------------------------------------
Changed files:
---------------------------------------------------

changed: /etc/crontab

---------------------------------------------------
Detailed information about changes:
---------------------------------------------------


File: /etc/crontab
 SHA256   : Wec/N3gzLCJwoWVE3RMzclv50BITEZtt , JZ7viDfit0RrfLKTDL3G20kxkxLdfWrH
```

remove the test file
```
rm /usr/local/nginx/new_file
```


## (Optional) Triggering ModSec IPS - watching it live! 

In another terminal window, watch the modsec audit log file fill up, detect and prevent actions:

```bash
tail -f /var/log/modsec_audit.log
```


**Example Action 1, Launch an example SQL injection attack:**
 
```bash
curl "https://mydomain.com/products?category=Gifts'+OR+1=1--" 
```

Action 1 modsec audit log output shows that sql injection attack was attempted, but blocked:
```
GET /products?category=Gifts'+OR+1=1-- HTTP/1.1
Host: mydomain.com
User-Agent: curl/7.67.0
Accept: */*

--ea977857-F--
HTTP/1.1 403 Forbidden
Content-Type: text/html
Content-Length: 146
Connection: keep-alive

--ea977857-H--
Message: Access denied with code 403 (phase 2). detected SQLi using libinjection with fingerprint 's&1c' [file "/usr/local/nginx/conf/modsecurity.conf"] [line "9552"] [id "942100"] [msg "SQL Injection Attack Detected via libinjection"] [data "Matched 
Data: s&1c found within ARGS:category: Gifts' OR 1=1--"] [severity "CRITICAL"] [ver "OWASP_CRS/3.2.0"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-sqli"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "OWASP_C
RS/WEB_ATTACK/SQL_INJECTION"] [tag "WASCTC/WASC-19"] [tag "OWASP_TOP_10/A1"] [tag "OWASP_AppSensor/CIE1"] [tag "PCI/6.5.2"]
Message: Audit log: Failed to lock global mutex: Permission denied
Action: Intercepted (phase 2)
Apache-Handler: IIS
Stopwatch: 1587668488000804 805796 (- - -)
Stopwatch2: 1587668488000804 805796; combined=1025, p1=356, p2=590, p3=0, p4=0, p5=78, sr=51, sw=1, l=0, gc=0
Producer: ModSecurity for nginx (STABLE)/2.9.0 (http://www.modsecurity.org/); OWASP_CRS/3.2.0.
Server: ModSecurity Standalone
Engine-Mode: "ENABLED"

--ea977857-Z--

```

**Example Trigger 2, prevent Cross Site Scripting (XSS) attacks:**


```bash
curl "https://mydomain.com/search?q=flowers+%3Cscript%3Eevil_script()%3C/script%3E"
```

OUTPUT of ModSec audit log, XXS attack blocked: 
```
GET /search?q=flowers+%3Cscript%3Eevil_script()%3C/script%3E HTTP/1.1
Host: mydomain.com
User-Agent: curl/7.67.0
Accept: */*

--c4068626-F--
HTTP/1.1 403 Forbidden
Content-Type: text/html
Content-Length: 146
Connection: keep-alive

--c4068626-H--
Message: Access denied with code 403 (phase 2). detected XSS using libinjection. [file "/usr/local/nginx/conf/modsecurity.conf"] [line "8522"] [id "941100"] [msg "XSS Attack Detected via libinjection"] [data "Matched Data: XSS data found within ARGS:q
: flowers <script>evil_script()</script>"] [severity "CRITICAL"] [ver "OWASP_CRS/3.2.0"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-xss"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "OWASP_CRS/WEB_ATTACK/X
SS"] [tag "WASCTC/WASC-8"] [tag "WASCTC/WASC-22"] [tag "OWASP_TOP_10/A3"] [tag "OWASP_AppSensor/IE1"] [tag "CAPEC-242"]
Message: Audit log: Failed to lock global mutex: Permission denied
Action: Intercepted (phase 2)
Apache-Handler: IIS
Stopwatch: 1587668843000633 634422 (- - -)
Stopwatch2: 1587668843000633 634422; combined=893, p1=365, p2=452, p3=0, p4=0, p5=75, sr=50, sw=1, l=0, gc=0
Producer: ModSecurity for nginx (STABLE)/2.9.0 (http://www.modsecurity.org/); OWASP_CRS/3.2.0.
Server: ModSecurity Standalone
Engine-Mode: "ENABLED"

--c4068626-Z--

```

**Example Trigger 3, prevent web requests trying to inject shell to HTTPS post:**
```bash
curl "https://mydomain.com/index.html?exec=/bin/bash"
```

OUTPUT of ModSec audit log, shell injection blocked: 
```
[09/Apr/2020:19:07:47 +0000] AcAcicwcAPWcAcA9tcAcAcpR 76.172.91.231 53443 127.0.0.1 80
--cf1e9827-B--
GET /index.html?exec=/bin/bash HTTP/1.1
Host: 34.212.70.58
User-Agent: curl/7.64.1
Accept: */*
--cf1e9827-F--
HTTP/1.1 403
Content-Type: text/html
Content-Length: 153
Connection: keep-alive
--cf1e9827-H--
Message: Access denied with code 403 (phase 2). Matched phrase "bin/bash" at ARGS:exec. [file "/usr/local/nginx/conf/modsecurity.conf"] [line "7421"] [id "932160"] [msg "Remote Command Execution: Unix Shell Code Found"] [data "Matched Data: bin/bash foun
d within ARGS:exec: /bin/bash"] [severity "CRITICAL"] [ver "OWASP_CRS/3.2.0"] [tag "application-multi"] [tag "language-shell"] [tag "platform-unix"] [tag "attack-rce"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "OWASP_CRS/WEB_ATTACK/COMMAND_INJECTIO
N"] [tag "WASCTC/WASC-31"] [tag "OWASP_TOP_10/A1"] [tag "PCI/6.5.2"]
Message: Audit log: Failed to lock global mutex: Permission denied
Action: Intercepted (phase 2)
Apache-Handler: IIS
Stopwatch: 1586459267000384 385701 (- - -)
Stopwatch2: 1586459267000384 385701; combined=856, p1=385, p2=391, p3=0, p4=0, p5=79, sr=91, sw=1, l=0, gc=0
Producer: ModSecurity for nginx (STABLE)/2.9.3 (http://www.modsecurity.org/); OWASP_CRS/3.2.0.
Server: ModSecurity Standalone
Engine-Mode: "ENABLED"
```




### Troubleshooting:

**Log Locations:**

- ModSec Audit log (block log): /var/log/modsec_audit.log

- Nginx Logs:  /usr/local/nginx/logs/

- Certbot logs:  /var/log/cert-renew.log
