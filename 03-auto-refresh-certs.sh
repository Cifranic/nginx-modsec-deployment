#!/bin/bash

# Create a log file for cert renewals 
touch /var/log/cert-renew.log

# Initiate a dry run to ensure it works.
echo "First run!" >> /var/log/cert-renew.log
letsencrypt renew --dry-run --agree-tos >> /var/log/cert-eenew.log

# Renew everyday at 3:10AM, per best practice to run daily. 
# this cronjob is stored at /etc/crontab
crontab -l | { cat; echo "12 3 * * *   letsencrypt renew >> /var/log/cert-renew.log"; } | crontab -

# restarting/reloading cron service
systemctl reload crond 
systemctl restart crond

