##SOurces
##https://geekflare.com/install-modsecurity-on-nginx/
## Tested on AWS AMI amazon-linux (centos variant) 

# A2 hosting centos box has apachi installed, bound to port 80. We must stop this process, so nginx can run on this port.  
systemctl stop httpd
systemctl disable httpd

#### INSTALL mod securiy, git
yum -y install epel-release 

# grab wget, git and openssl 
yum -y install wget git openssl-devel gcc-c++

# dependencies for making binaries from scratch 
yum -y install gcc make automake autoconf libtool pcre pcre-devel libxml2 libxml2-devel curl curl-devel httpd-devel

# create nginx user
useradd nginx

# move to home 
cd ~

# grab nginx source code (version 1.16.1 is most up to date)
wget https://nginx.org/download/nginx-1.16.1.tar.gz

# unzip
gunzip -c nginx-1.16.1.tar.gz| tar xvf -

#-----

# move to home
cd ~ 

# grab nginx branch of modsecuity (2.9.3)
git clone -b nginx_refactoring https://github.com/SpiderLabs/ModSecurity.git

# rename repo
mv ModSecurity modsecurity-2.9.3

# MIGHT BE NEEDED:
#git submodule init                                                                                                                                      
#git submodule update

# go into modsec dir and build binary
cd ~/modsecurity-2.9.3
./autogen.sh
./configure --enable-standalone-module
make

#-----

# go into nginx dir and build binary 
cd ~/nginx-1.16.1/
./configure --add-module=../modsecurity-2.9.3/nginx/modsecurity --with-http_ssl_module --with-http_stub_status_module 
make
make install

# copy important files to nginx web-root
cp /root/modsecurity-2.9.3/modsecurity.conf-recommended /usr/local/nginx/conf/
cp /root/modsecurity-2.9.3/unicode.mapping /usr/local/nginx/conf/

# rename file to modsecurity.conf
mv /usr/local/nginx/conf/modsecurity.conf-recommended /usr/local/nginx/conf/modsecurity.conf

# copy service file for systemd 
cp /root/nginx-modsec-deployment/system-files/nginx.service /usr/lib/systemd/system/

# reload systemd 
systemctl daemon-reload 

# start nginx
systemctl start nginx 

# show its on:
systemctl status nginx
sleep 5

# ensure nginx starts on boot:
systemctl enable nginx

# ensure that modsecurity plugin for nginx works:  "modsecurity successful"
printf "Below, you should see: ModSecurity for nginx (STABLE)/2.9.3 (http://www.modsecurity.org/) configured.\n\n"
tail /usr/local/nginx/logs/error.log 
sleep 20

### CONFIGURE Mod-security and add OWASP rules 
cd ~
git clone https://github.com/SpiderLabs/owasp-modsecurity-crs.git

# create one BIG rules file in nginx/conf directory
cat ~/owasp-modsecurity-crs/crs-setup.conf.example ~/owasp-modsecurity-crs/rules/*.conf >> /usr/local/nginx/conf/modsecurity.conf

# copy over rule to nginx/conf directory
cp ~/owasp-modsecurity-crs/rules/*.data /usr/local/nginx/conf/

# copy proper nginx.conf file 
cp /root/nginx-modsec-deployment/system-files/nginx.conf /usr/local/nginx/conf/nginx.conf


# What the next command does: 
    # enables Intrusion Prevention
    # Changes file: /usr/local/nginx/conf/modsecurity.conf
       #FROM: "SecRuleEngine Detect" 
       #TO:   "SecRuleEngine On"

       #FROM: SecDefaultAction "phase:1,log,auditlog,pass" TO 
       #TO:   SecDefaultAction "phase:1,deny,log"

       #NOTE:  Changing SecDefaultAction to deny, and log is where modsec is instructed to prevent intrusions
cp /root/nginx-modsec-deployment/system-files/modsecurity.conf /usr/local/nginx/conf/modsecurity.conf

# restart nginx after modsecurity conf file is updated 
systemctl restart nginx

# install apache to put behind nginx proxy
yum -y install httpd 

# update the apache config
sed -i 's/Listen 80/Listen 8080/g' /etc/httpd/conf/httpd.conf

# start up apache 
systemctl start httpd
systemctl enable httpd


